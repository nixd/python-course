---
title: 'Chapter 1: Introduction to Python'
description:
  'This chapter will introduce you to the basic concepts of Python.'
prev: null
next: /chapter2
type: chapter
id: 1
---

<exercise id="1" title="Hello World!">


The content of this chapter is heavily inspired by [A byte of Python](https://python.swaroopch.com/).

## What is Python

Python is a popular programming language. It was created by Guido van Rossum, and released in 1991. It is used for

* Web development (server-side)
* Software development
* Mathematics
* Automation
* Data Science

## Why Python?

* Python works on different platforms (Windows, Mac, Linux, Raspberry Pi, etc).
* Python has a simple syntax similar to the English language.
* Python has syntax that allows developers to write programs with fewer lines than some other programming languages.
* Python runs on an interpreter system, meaning that code can be executed as soon as it is written. This means that prototyping can be very quick.
* Python can be treated in a procedural way, an object-orientated way or a functional way.


## Python Syntax compared to other programming languages

* Python was designed for readability, and has some similarities to the English language with influence from mathematics.
* Python uses new lines to complete a command, as opposed to other programming languages which often use semicolons or parentheses.
* Python relies on indentation, using whitespace, to define scope; such as the scope of loops, functions and classes. Other programming languages often use curly-brackets for this purpose.


<codeblock id='hello_world'>
</codeblock>

</exercise>

<exercise id="2" title="Objects and data structures" type="slides">

<slides source="objects_and_data_structures"></slides>

</exercise>


<exercise id="3" title="Exceptions" type="slides">

<slides source="exceptions"></slides>

</exercise>

<exercise id="4" title="Methods and functions" type="slides">

<slides source="methods_and_functions"></slides>

</exercise>

<exercise id="5" title="Statements" type="slides">

<slides source="statements"></slides>

</exercise>

<exercise id="6" title="Lists and list comprehensions" type="slides">

<slides source="lists"></slides>

</exercise>

<exercise id="7" title="Printing and formatting" type="slides">

<slides source="printing_and_formatting"></slides>

</exercise>

<exercise id="8" title="Datetime" type="slides">

<slides source="datetime"></slides>

</exercise>


<exercise id="9" title="Modules and packages" type="slides">

<slides source="modules_and_packages"></slides>

</exercise>

<exercise id="10" title="Assignment" type="slides">

<slides source="basic_assignment"></slides>

</exercise>
