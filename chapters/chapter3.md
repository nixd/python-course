---
title: 'Chapter 3: From zero to hero'
description:
  'This chapter will demonstrate how to get data online and do some analysis on the data.'
prev: /chapter2
next: null
type: chapter
id: 3
---

<exercise id="1" title="F1 data">

In the following [notebook](https://mybinder.org/v2/gl/nixd%2Fpython-course/master?filepath=notebooks/formula_one.ipynb) I'll demonstrate how to get data from Wikipedia and do some analysis on the data.

</exercise>


<exercise id="2" title="Danish election 2019">

In the following [notebook](https://mybinder.org/v2/gl/nixd%2Fpython-course/master?filepath=notebooks/danish_election.ipynb) you should demonstrate your skills on a similar case.


</exercise>
