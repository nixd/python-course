---
type: slides
---

# Modules and Packages

---

# Modules

You have seen how you can reuse code in your program by defining functions once. What if you wanted to reuse a number of functions in other programs that you write? As you might have guessed, the answer is modules.

A module can be *imported* by another program to make use of its functionality. This is how we can use the Python standard library as well. First, we will see how to use the standard library modules.


```python
import sys

print('The PYTHONPATH is', sys.path, '\n')
```

```out
The PYTHONPATH is ['/tmp/py',
# many entries here, not shown here
'/Library/Python/3.7/site-packages',
'/usr/local/lib/python3.7/site-packages']
```


Notes:

First, we import the `sys` module using the import statement. Basically, this translates to us telling Python that we want to use this module. The `sys` module contains functionality related to the Python interpreter and its environment i.e. the system.

When Python executes the `import sys` statement, it looks for the sys module. In this case, it is one of the built-in modules, and hence Python knows where to find it.

Note that the initialization is done only the first time that we import a module.

The `sys.path` contains the list of directory names where modules are imported from. Observe that the first string in `sys.path` is empty - this empty string indicates that the current directory is also part of the `sys.pat`h which is same as the `PYTHONPATH` environment variable. This means that you can directly import modules located in the current directory. Otherwise, you will have to place your module in one of the directories listed in `sys.path`.

The current directory is the directory from which the program is launched. Run `import os; print(os.getcwd())` to find out the current directory of your program.

---

# A modules `__name__`

Every module has a name and statements in a module can find out the name of their module. This is handy for the particular purpose of figuring out whether the module is being *run standalone* or *being imported*. As mentioned previously, when a module is imported for the first time, the code it contains gets executed. We can use this to make the module behave in different ways depending on whether it is being used by itself or being imported from another module. This can be achieved using the `__name__` attribute of the module.

```python
if __name__ == '__main__':
    print('This program is being run by itself')
else:
    print('I am being imported from another module')
```

```out
$ python module_using_name.py
This program is being run by itself

$ python
>>> import module_using_name
I am being imported from another module
```

---

# Making your own module

Creating your own modules is easy. Every Python program is also a module. You just have to make sure it has a `.py` extension. The following example should make it clear.

Example (save as `mymodule.py`):

```python
def say_hi():
    print('Hi, this is mymodule speaking.')

__version__ = '0.1'
```

Running python from the location of the `mymodule.py`

```python
import mymodule

mymodule.say_hi()
print('Version', mymodule.__version__)
```

```out
Hi, this is mymodule speaking.
Version 0.1
```

---


# The `from .. import` statement

In the previous example you could have imported `say_hi` and `__version__` directory from `mymodule` instead.

```python
from mymodule import say_hi, __version__

say_hi()
print('Version', __version__)
```

```out
Hi, this is mymodule speaking.
Version 0.1
```

Notes:
 
*WARNING*: In general, avoid using the `from .. import` statement, use the import statement instead. This is because your program will avoid name clashes and will be more readable. Especially `from .. import *` should always be avoided.


---

# Packages

Let's say you want to create a package called 'world' with subpackages 'asia', 'africa', etc. and these subpackages in turn contain modules like 'india', 'madagascar', etc.

This is how you would structure the folders:

```out
- <some folder present in the sys.path>/
    - world/
        - __init__.py
        - asia/
            - __init__.py
            - india/
                - __init__.py
                - foo.py
        - africa/
            - __init__.py
            - madagascar/
                - __init__.py
                - bar.py
```

Notes: 


By now, you must have started observing the hierarchy of organizing your programs. Variables usually go inside functions. Functions and global variables usually go inside modules. What if you wanted to organize modules? That's where *packages* come into the picture.

Packages are just folders of modules with a special `__init__.py` file that indicates to Python that this folder is special because it contains Python modules.

Subpackages are simply a package inside another package. Hence ` asia`, `africa`, `madagascar` and `india` are all subpackages.


---

# <a href="https://www.kaggle.com/kernels/fork/1275190" target="_blank">Lets do some exercises on imports</a>
