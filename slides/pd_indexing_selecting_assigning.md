---
type: slides
---

# Indexing, selecting and assigning

---

# Selecting columns by name

Consider the following `dataframe`

```python
import pandas as pd
df = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]}, index=['first', 'second', 'last'])
df
```

```out
        A  B  C
first   1  4  7
second  2  5  8
last    3  6  9
```

Selecting a single column, which yields a `Series` is simple

```python
df['A']  # alternatively: df.A
```

```out
first     1
second    2
last      3
Name: A, dtype: int64
```

Notes:

Note that the index are kept in the `series` object.

---

# Selecting by position

Selecting via `[]`, which slices the rows.

```python
df[:2]
```

```out
        A  B  C
first   1  4  7
second  2  5  8
```


---

# Selection by label



```python
df.loc['first']
```

```out
A    1
B    4
C    7
Name: first, dtype: int64
```

Selecting on a multi-axis by label:

```python
df.loc['second':, ['A', 'B']]
```

```out
        A  B
second  2  5
last    3  6
```


And just return the scalar at a position

```python
df.loc['first', 'A']
```

```out
1
```


---

# Selection by position


```python
df.iloc[2]
```

```out
A    3
B    6
C    9
Name: last, dtype: int64
```

By lists of integer position locations

```python
df.iloc[:2, [0, 2]]
```

```out
        A  C
first   1  7
second  2  8
```

And just return the scalar at a position

```python
df.iloc[0, 0]
```

```out
1
```


---

# Boolean indexing

Using a single column’s values to select data.

```python
df[df.A > 2]
```

```out
      A  B  C
last  3  6  9
```

Selecting values from a DataFrame where a boolean condition is met.

```python
df[df > 4]
```

```out
         A    B  C
first  NaN  NaN  7
second NaN  5.0  8
last   NaN  6.0  9
```


---

# Assigning

Setting a new column automatically aligns the data by the indexes.

```python
df['D'] = [10, 11, 12]
df
```

```out
        A  B  C   D
first   1  4  7  10
second  2  5  8  11
last    3  6  9  12
```

Setting values by label:

```python
df.at['first', 'A'] = 0
df
```

```out
        A  B  C   D
first   0  4  7  10
second  2  5  8  11
last    3  6  9  12
```



---

# Assigning

Setting values by position:

```python
df.iat[0, 1] = 0
df
```

```out
        A  B  C   D
first   0  0  7  10
second  2  5  8  11
last    3  6  9  12
```

Setting by assigning with a array:

```python
df.loc[:'second', 'D'] = [-2, -1]
```

```out
        A  B  C   D
first   0  0  7  -2
second  2  5  8  -1
last    3  6  9  12
```



---

# Assigning

A `where` operation with setting.

```python
df[df>0] = df * 2
df
```

```out
        A   B   C   D
first   0   0  14  -2
second  4  10  16  -1
last    6  12  18  24
```

Setting by assigning with a array:

```python
df.loc[:'second', 'D'] = [-2, -1]
```

```out
        A  B  C   D
first   0  0  7  -2
second  2  5  8  -1
last    3  6  9  12
```





---

# <a href="https://www.kaggle.com/kernels/fork/587910" target="_blank">Now go solve these exercises</a>
