---
type: slides
---

# Summary functions and maps

---

# Operations

Operations in general *exclude* missing data. Performing a descriptive statistic:


```python
df = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]}, index=['first', 'second', 'last'])
df.mean()
```

```out
A    2.0
B    5.0
C    8.0
dtype: float64
```

Or on the other axis

```python
df.mean('columns')  # alternately df.mean(1) 
```

```out
first     4.0
second    5.0
last      6.0
dtype: float64
```

---

# Describe


```python
df.describe()
```

```out
         A    B    C
count  3.0  3.0  3.0
mean   2.0  5.0  8.0
std    1.0  1.0  1.0
min    1.0  4.0  7.0
25%    1.5  4.5  7.5
50%    2.0  5.0  8.0
75%    2.5  5.5  8.5
max    3.0  6.0  9.0
```


---

# Value counts


```python
pd.Series(['A', 'A', 'B', 'C', 'A', 'C', 'A']).value_counts()
```

```out
A    4
C    2
B    1
dtype: int64
```




---

# Unique values


```python
pd.Series(['A', 'A', 'B', 'C', 'A', 'C', 'A']).unique()
```

```out
array(['A', 'B', 'C'], dtype=object)
```


```python
pd.Series(['A', 'A', 'B', 'C', 'A', 'C', 'A']).nunique()
```

```out
3
```


---

# Map

To apply a function to each value in a series you can use `.map`


```python
pd.Series(['A', 'A', 'B', 'C', 'A', 'C', 'A']).map(lambda x: x == 'A')
```

```out
0     True
1     True
2    False
3    False
4     True
5    False
6     True
dtype: bool
```


---

# Apply

To apply a function to each column (or row) in a dataframe you can use `.apply`


```python
df = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6], 'C': [7, 8, 9]}, index=['first', 'second', 'last'])
df.apply(lambda x: max(x))
```

```out
A    3
B    6
C    9
dtype: int64
```

Apply row-wise

```python
df.apply(lambda x: max(x), axis='columns')  # or df.apply(lambda x: max(x), axis=1)
```

```out
first     7
second    8
last      9
dtype: int64
```


---

# Applymap

To apply a function to every scalar in a dataframe you can use `.applymap`

```python
df.applymap(lambda x: 2 < x < 6)
```

```out
            A      B      C
first   False   True  False
second  False   True  False
last     True  False  False
```


---

# <a href="https://www.kaggle.com/kernels/fork/595524" target="_blank">Now go solve these exercises</a>
