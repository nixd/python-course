---
type: slides
---

# Date and time

---

# `datetime` library


```python
import datetime as dt
now = dt.datetime.now()
today = dt.date.today()
print(now)  # format depends on the system
print(today)  # format depends on the system
```

```out
2019-05-21 16:24:25.605370
2019-05-21
```

Notes:

The `datetime` package can be used for date and timestamp objects.


---

# dates and strings


```python
some_datetime = dt.datetime.strptime('1986-04-21T21:29:26+0100', '%Y-%m-%dT%H:%M:%S%z')
print(some_datetime.strftime('%d %b %y'))
```


```out
21 Apr 86
```


Notes:

You can convert a string into a `datetime` using `strptime` and convert a `datetime`into a `str` using `strftime`.


---

# Attributes

Handy datetime attributes

```python
some_datetime = dt.datetime.strptime('1986-04-21T21:29:26+0100', '%Y-%m-%dT%H:%M:%S%z')
print(some_datetime.year, some_datetime.month, some_datetime.day, some_datetime.hour, some_datetime.minute, some_datetime.second)
```


```out
1986 4 21 21 29 26
```



---

# Manipulation with dates

`timedelta` is a duration expressing the difference between two dates.

```python
print(dt.datetime(2019, 1, 1) + dt.timedelta(days = 100))
```


```out
2019-04-11 00:00:00
```

And you can compare dates

```python
x = dt.datetime(2019, 1, 1)
print(x + dt.timedelta(days = 100) > x)
```


```out
True
```

---

# Proceed to next subject
