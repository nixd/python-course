---
type: slides
---

# Creating, reading and writing

---

# Series

Creating a `Series` by passing a list of values, letting pandas create a default integer index:

```python
pd.Series([1, None, 8])
```

```out
0    1.0
1    NaN
2    8.0
```

or by a dict

```python
pd.Series({'A': 2, 'B': 3})
```

```out
A    2
B    3
dtype: int64
```

---

# Dataframe

Creating a `DataFrame` by passing a nested array, specified index and labeled columns:

```python
pd.DataFrame([[1, 3], [2, 4]], index=['a', 'b'], columns=list('AB'))
```

```out
   A  B
a  1  3
b  2  4
```

or by a dict

```python
pd.DataFrame({'A': [1, 2], 'B': [3, 4]}, index=['a', 'b'])
```

```out
   A  B
a  1  3
b  2  4
```

---

# Reading and writing data

Pandas can read from a lot of different sources. For a complete list see [here](https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html).

It is very simple reading a csv

```python
pd.read_csv('sample.csv')
```

or from a SQL database (replace `conn` with a connection to the database of interest).

```python
import sqlite3
conn = sqlite3.connect('localhost')
pd.read_sql('select * from sales', conn)
```

Writing data is just a easy. Instead of `read_xxx` then you use `to_xxx`



---

# <a href="https://www.kaggle.com/kernels/fork/587970" target="_blank">Now go solve these exercises</a>
