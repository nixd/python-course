---
type: slides
---

# Combining

---

# Combining/merging

`pandas` provides various facilities for easily combining together `Series` and `DataFrame`.

## Concat

You can concat on top of eachother 


```python
pd.concat([pd.DataFrame(np.random.randn(2, 2)), pd.DataFrame(np.random.randn(4, 2))])
```

```out
          0         1
0  0.242682 -1.104338
1  0.206996 -0.078473
0 -0.248264 -1.590963
1  0.396766  2.044044
2  0.154276 -1.777991
3  1.897967  0.848274
```

and beside eachother


```python
pd.concat([pd.DataFrame(np.random.randn(2, 2)), pd.DataFrame(np.random.randn(2, 4))], axis=1)
```

```out
          0         1         0         1         2         3
0 -0.036112  0.035549 -1.050142 -1.038552 -1.381191 -0.406034
1 -0.268302 -1.151809 -1.290980 -0.232643  0.186946 -1.412246
```

---

# Join


```python
left = pd.DataFrame({'key': ['foo', 'foo'], 'lval': [1, 2]})
left
```

```out
   key  lval
0  foo     1
1  foo     2
```

```python
right = pd.DataFrame({'key': ['foo', 'foo'], 'rval': [4, 5]})
right
```

```out
   key  rval
0  foo     4
1  foo     5
```

```python
pd.merge(left, right, on='key')
```

```out
   key  lval  rval
0  foo     1     4
1  foo     1     5
2  foo     2     4
3  foo     2     5
```

Notes: 


`pandas` supports SQL style merges


---

# Append

Append rows to a dataframe

```python
df = pd.DataFrame(np.random.randn(2, 3), columns=['A', 'B', 'C'])
df
```


```out
          A         B         C
0  0.746597  0.072645  0.247841
1  0.599127  0.477722  0.623712
```

```python
df.append(df.iloc[0], ignore_index=True)
```


```out
          A         B         C
0  0.746597  0.072645  0.247841
1  0.599127  0.477722  0.623712
2  0.746597  0.072645  0.247841
```


---

# <a href="https://www.kaggle.com/kernels/fork/638064" target="_blank">Now go solve these exercises</a>
