---
type: slides
---

# Exceptions

---

# Errors

Exceptions occur when exceptional situations occur in your program. For example, what if you are going to read a file and the file does not exist? Or what if you accidentally deleted it when the program was running? Such situations are handled using *exceptions*.

Similarly, what if your program had some invalid statements? This is handled by Python which *raises* its hands and tells you there is an *error*.


```python
>>> Print("Hello World")
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'Print' is not defined
>>> print("Hello World")
Hello World
```

Notes:

Observe that a `NameError` is raised and also the location where the error was detected is printed. This is what an *error handler* for this error does.


---

# Handling exceptions

We can handle exceptions using the `try..except` statement. We basically put our usual statements within the try-block and put all our error handlers in the except-block.

```python
try:
    text = input('Enter something --> ')
except KeyboardInterrupt:
    print('You cancelled the operation.')
else:
    print('You entered {}'.format(text))
```

```out
# Press ctrl + c
$ python exceptions_handle.py
Enter something --> ^CYou cancelled the operation.

$ python exceptions_handle.py
Enter something --> No exceptions
You entered No exceptions
```

Notes:

We put all the statements that might raise exceptions/errors inside the `try` block and then put handlers for the appropriate errors/exceptions in the `except` clause/block. The `except` clause can handle a single specified error or exception, or a parenthesized list of errors/exceptions. If no names of errors or exceptions are supplied, it will handle *all* errors and exceptions.

Note that there has to be at least one except clause associated with every `try` clause. Otherwise, what's the point of having a try block?

If any error or exception is not handled, then the default Python handler is called which just stops the execution of the program and prints an error message. We have already seen this in action above.

You can also have an `else` clause associated with a `try..except` block. The else clause is executed if no exception occurs.


---

# Raising exceptions



```python
class ShortInputException(Exception):
    '''A user-defined exception class.'''
    def __init__(self, length, atleast):
        Exception.__init__(self)
        self.length = length
        self.atleast = atleast

try:
    text = input('Enter something --> ')
    if len(text) < 3:
        raise ShortInputException(len(text), 3)
except ShortInputException as ex:
    print('ShortInputException: The input was {0} long, expected at least {1}'.format(ex.length, ex.atleast))
else:
    print('No exception was raised.')
```

```out
Enter something --> a
ShortInputException: The input was 1 long, expected at least 3

Enter something --> abc
No exception was raised.
```

Notes:

You can `raise` exceptions using the raise statement by providing the name of the error/exception and the exception object that is to be *thrown*.

The error or exception that you can raise should be a class which directly or indirectly must be a derived class of the `Exception` class.


---

# Proceed to next subject
