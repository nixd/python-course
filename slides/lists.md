---
type: slides
---

# Lists and list comprehensions

---

# Lists


Lists in Python represent ordered sequences of values.

```python
primes = [2, 3, 5, 7]
```

A list can contain a mix of different types of variables:

```python
my_favourite_things = [32, 'raindrops on roses', help, primes]
```

---

# Indexing


You can access individual list elements with square brackets.

Which planet is closest to the sun? Python uses zero-based indexing, so the first element has index 0.

```python
planets = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune']
planets[0]
```

```out
'Mercury'
```

Which planet is furthest from the sun?

```python
planets[-1]
```

```out
'Neptune'
```

---

# Slicing

What are the first three planets? We can answer this question using slicing:

```python
planets[0:3]
```

```out
['Mercury', 'Venus', 'Earth']
```

```python
planets[:3]
```

```out
['Mercury', 'Venus', 'Earth']
```


---

# Changing lists

Lists are "mutable", meaning they can be modified "in place".


```python
planets[3] = 'Malacandra'
planets
```

```out
['Mercury',
 'Venus',
 'Earth',
 'Malacandra',
 'Jupiter',
 'Saturn',
 'Uranus',
 'Neptune']
```

---

# List functions

* `len` gives the length of a list.
* `sorted` returns a sorted version of a list.
* `min`, `max` and `sum` does what you might expect.

```python
print(len([5, 2, 3]))
print(sorted([1, 3, 2]))
print(min([1, 2, 3]))
print(max([1, 2, 3]))
print(sum([1, 2, 3]))
```

```out
3
[1, 2, 3]
1
3
6
```

---

# List methods

* `append` modifies a list by adding an item to the end.
* `pop` removes and returns the last element of a list (last by default)
* `index` return first index of value.

```python
l = [5, 2, 3]
l.append('tmp')
print(l)
print(l.pop())
print(l)
print(l.index(3))
```

```out
[5, 2, 3, 'tmp']
tmp
[5, 2, 3]
2
```

---

# Tuples

Tuples are almost exactly the same as lists. They differ in just two ways. 

1. The syntax for creating them uses parentheses instead of square brackets.

```python
(1, 2, 3)
```

```out
(1, 2, 3)
```

2. They cannot be modified (they are immutable).

```python
t = (1, 2, 3)
t[0] = 5
```

```out
Traceback (most recent call last):
  File "<input>", line 2, in <module>
TypeError: 'tuple' object does not support item assignment
```

---

# List comprehension

List comprehensions are one of Python's most beloved and unique features. The easiest way to understand them is probably to just look at a few examples:

```python
squares = [n**2 for n in range(10)]
squares
```

```out
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
```

```python
squares = []
for n in range(10):
    squares.append(n**2)
squares
```

```out
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
```

```python
planets = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune']
[planet.upper() + '!' for planet in planets if len(planet) < 6]
```

```out
['VENUS!', 'EARTH!', 'MARS!']
```




---


## <a href="https://www.kaggle.com/kernels/fork/1275173" target="_blank">Lets do some exercises on lists</a>


## <a href="https://www.kaggle.com/kernels/fork/1275177" target="_blank">... and loops and list comprehensions</a>
