---
type: slides
---

# Methods and functions

---

# Functions

Functions are reusable pieces of programs. They allow you to give a name to a block of statements, allowing you to run that block using the specified name anywhere in your program and any number of times. This is known as *calling* the function.

The function concept is probably the most important building block of any non-trivial software (in any programming language), so we will explore various aspects of functions in this chapter.


```python
def say_hello():
    # block belonging to the function
    print('Hello World!')
# end of function


say_hello()  # call the function
say_hello()  # call the function again
```

```out
Hello World!
Hello World!
```

Notes:


Functions are defined using the `def` keyword followed by an identifier name for the function. Then follows a pair of parentheses which may enclose some names of variables, and by the final colon that ends the line. Next follows the block of statements that are part of this function. An example will show that this is actually very simple.


---

# Function parameters


A function can take parameters, which are values you supply to the function so that the function can do something utilising those values. These parameters are just like variables except that the values of these variables are defined when we call the function and are already assigned values when the function runs.


```python
def print_max(a, b):
    if a > b:
        print(a, 'is maximum')
    elif a == b:
        print(a, 'is equal to', b)
    else:
        print(b, 'is maximum')

print_max(3, 4)  # directly pass literal values

x = 5
y = 7

print_max(x, y)  # pass variables as arguments
```


```out
4 is maximum
7 is maximum
```

Notes: 

Parameters are specified within the pair of parentheses in the function definition, separated by commas. When we call the function, we supply the values in the same way. Note the terminology used - the names given in the function definition are called parameters whereas the values you supply in the function call are called arguments.


---


# Local variables

When you declare variables inside a function definition, they are not related in any way to other variables with the same names used outside the function - i.e. variable names are local to the function. This is called the *scope* of the variable. All variables have the scope of the block they are declared in starting from the point of definition of the name.

```python
x = 50
def func(x):
    print('x is', x)
    x = 2
    print('Changed local x to', x)
func(x)
print('x is still', x)
```

```out
x is 50
Changed local x to 2
x is still 50
```

Notes:

If you want to assign a value to a name defined at the top level of the program (i.e. not inside any kind of scope such as functions or classes), then you have to tell Python that the name is not local, but it is global. We do this using the `global` statement. However, this is not encouraged and should be avoided since it becomes unclear to the reader of the program as to where that variable's definition is



---


# Default Argument Values

For some functions, you may want to make some parameters *optional* and use default values in case the user does not want to provide values for them. This is done with the help of default argument values.


```python
def say(message, times=1):
    print(message * times)

say('Hello')
say('World', 5)
```

```out
Hello
WorldWorldWorldWorldWorld
```

Notes:

Note that the default argument value should be a constant. More precisely, the default argument value should be immutable

---


# Keyword Arguments

If you have some functions with many parameters and you want to specify only some of them, then you can give values for such parameters by naming them - this is called *keyword arguments* - we use the name (keyword) instead of the position (which we have been using all along) to specify the arguments to the function.

```python
def func(a, b=5, c=10):
    print('a is', a, 'and b is', b, 'and c is', c)

func(3, 7)
func(25, c=24)
func(c=50, a=100)
```

```out
a is 3 and b is 7 and c is 10
a is 25 and b is 5 and c is 24
a is 100 and b is 5 and c is 50
```

Notes:

There are two advantages - one, using the function is easier since we do not need to worry about the order of the arguments. Two, we can give values to only those parameters to which we want to, provided that the other parameters have default argument values.


---

# VarArgs parameters

Sometimes you might want to define a function that can take *any* number of parameters, i.e. *var*iable number of *arg*uments, this can be achieved by using asterisk symbol


```python
def total(a=5, *numbers, **phonebook):
    print('a', a)

    #iterate through all the items in tuple
    for single_item in numbers:
        print('single_item', single_item)

    #iterate through all the items in dictionary    
    for first_part, second_part in phonebook.items():
        print(first_part,second_part)

total(10, 1, 2, Jack=1123, John=2231, Inge=1560)
```

```out
a 10
single_item 1
single_item 2
Jack 1123
John 2231
Inge 1560
```


---


# The `return` statement


The `return` statement is used to return from a function i.e. break out of the function. We can optionally return a value from the function as well.

```python
def maximum(x, y):
    if x >= y:
        return x
    else:
        return y

print(maximum(2, 3))
```

```out
3
```


---

# DocStrings

```python
def print_max(x, y):
    """
    Prints the maximum of two numbers. The two values must be integers.

    :param x: int
    :param y: int
    :return: None
    """
    if x > y:
        print(x, 'is maximum')
    else:
        print(y, 'is maximum')
        
help(print_max)
```

```out
Help on function print_max in module __main__:

print_max(x, y)
    Prints the maximum of two numbers. The two values must be integers.
    
    :param x: int
    :param y: int
    :return: None
```

Notes:



Python has a nifty feature called documentation strings, usually referred to by its shorter name docstrings. DocStrings are an important tool that you should make use of since it helps to document the program better and makes it easier to understand. We can even get the docstring back from, say a function, when the program is actually running!


---

# Typing

```python
def get_max(x: int, y: int) -> int:
    if x > y:
        return x
    else:
        return y
        
help(get_max)
```

```out
Help on function get_max in module __main__:

get_max(x:int, y:int) -> int
```

Notes:



Recently (since `python3.6`) it is possible to add type hints to arguments, variables, return types etc.. It is syntactic sugar that empowers modern IDEs to help with auto completion. I use it for everything and can often replace docstrings if the function names and variables are self-explanatory! 



---

# <a href="https://www.kaggle.com/kernels/fork/1275158" target="_blank">Lets try some exercises on functions</a>
