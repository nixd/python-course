---
type: slides
---

# Assignment

---


Create a simple temperature converter, i.e. fahrenheit to celsius and celcius to fahrenheit

```python
fahrenheit_to_celsius(32)
```

```out
0
```


```python
celsius_to_fahrenheit(0)
```

```out
32
```

Try to use this to determine when the fahrenheit and celcius value are equal


### Advanced

Create a more general converter that allows something like



```python
unit = Unit("32f")  # or Unit("0c")
print(f"F: {unit.fahrenheit}\nC: {unit.celcius}\nK: {unit.kelvin}")
```

```out
F: 32
C: 0
K: 273
```

