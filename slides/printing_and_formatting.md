---
type: slides
---

# Printing and formatting


---

# Split and join

Use `split` to transform a `str` into a `list` and `join` to transform a `list` of `str` into `str`.

```python
full_name = 'Nicolai Bjerre Pedersen'
print('\n'.join(full_name.split(' ')))
```

```out
Nicolai
Bjerre
Pedersen
```



---

# Formatting a string



A pythonic way to extract the list into multiple variables with explanatory ways. Use `format` to insert variables into 
a `str`

```python
first_name, middle_name, last_name = 'Nicolai Bjerre Pedersen'.split(' ')
print('First name: {first_name}\nMiddle name: {middle_name}\nLast name: {last_name}'.format(
    first_name = first_name, middle_name = middle_name, last_name = last_name))
```

```out
First name: Nicolai
Middle name: Bjerre
Last name: Pedersen
```


---

# Formatting numbers


You can also use `format` to print numbers in a convenient format. 


```python
print('Add thousand separator: {:,d}'.format(45120000001))
print('Round to two decimals: {:.2f}'.format(0.321213))
print(f'Or combine the two: {4512000000.321213:,.2f}')  # this is called f strings (>= python3.6)
print(f'Or add leading zeroes: {str(1).zfill(3)}')
```

```out
Add thousand separator: 45,120,000,001
Round to two decimals: 0.32
Or combine the two: 4,512,000,000.32
Or add leading zeroes: 001
```



---

# <a href="https://www.kaggle.com/kernels/fork/1275185" target="_blank">Lets do some exercises on strings and dictionaries</a>
