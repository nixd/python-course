---
type: slides
---

# Objects and Data Structures


---

# Builtin objects

A built-in type in Python exists as part of the Python programming language. This means that to use the built-in type, you don't have to build it yourself or call it by importing a module (as you would for other objects); rather, a built-in type always is available for you to use as you please. The most common types in Python includes strings, integers, dictionaries, lists, tuples, sets and floats.

# Immutable builtin types

* `int`, `float`
* `str`
* `tuple`

# Mutable builtin types

* `list`
* `dict`
* `set`

Notes: Simple put, a mutable object can be changed after it is created, and an immutable object can’t.

---

# Checking the type

If you wish to determine what type an object is you can simply use `type` method

```python
print(type(None))
print(type('This is a string'))
print(type(1))
print(type(1.0))
print(type(False))
```

```out
<class 'NoneType'>
<class 'str'>
<class 'int'>
<class 'float'>
<class 'bool'>
```
---

# "Non-compatible" types 

You can't take the sum between a string and an integer for instance

```python
"2" + 2
```

```out
TypeError: cannot concatenate 'str' and 'int' objects
```

Not that if we have an `int` first then the error message is different

```python
2 + "2"
```

```out
TypeError: unsupported operand type(s) for +: 'int' and 'str'
```

Instead we can

```python
"2" + "2"  # '22'
int("2" + "2")  # 22
int("2") + int("2")  # 4
2 + 2  # 4
```

---

# Data structures

*Data structures* are basically just that - they are structures which can hold some data together. In other words, they are used to store a collection of related data.

There are four built-in data structures in Python - *list*, *tuple*, *dictionary* and *set*. We will see how to use each of them and how they make life easier for us.


```python
list_example = [1, 'a', 2, 'b']  # alternatively: list([1, 'a', 2, 'b'])
tuple_example = (1, 'a', 2, 'b')  # alternatively: tuple([1, 'a', 2, 'b'])
dict_example = {'a': [1, 2]}  # alternatively: dict(a=[1, 2])
set_example = {1, 'a', 2, 'b'}  # alternatively: set([1, 'a', 2, 'b'])
isinstance(dict_example, dict)
```

```out
True
```


Notes: 

A `list` is a data structure that holds an ordered collection of items i.e. you can store a sequence of items in a list. 

A `tuple` is like a list but one major feature of tuples is that they are immutable, i.e. you cannot modify tuples.

A `dict` is like an address-book where you can find the address or contact details of a person by knowing only his/her name i.e. we associate keys (name) with values (details). Note that the key must be unique just like you cannot find out the correct information if you have two persons with the exact same name.

A `set` is an unordered collection of simple objects. These are used when the existence of an object in a collection is more important than the order or how many times it occurs.


---

# <a href="https://www.kaggle.com/kernels/fork/1275163" target="_blank">Lets try some basic exercises</a>
