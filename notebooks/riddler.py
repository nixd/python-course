def print_separator():
    print('\n{}\n'.format('=' * 40))


class Riddle:

    def __init__(self, question, solution, hints=None):
        self.question = '{}\n'.format(question.strip())
        self.solution = solution
        self.hints = [] if hints is None else ['{}\n'.format(hint.strip()) for hint in hints]

    def is_correct(self, answer):
        correct = self.solution.lower() == answer.lower()
        if correct:
            print('Correct!')
        return correct

    def give_hint(self):
        if len(self.hints) > 0:
            print(self.hints.pop(0), end='\n')
        else:
            print('I have no more hints for you.', end='\n')

    def start(self):
        answer = input(self.question)
        n_guesses = 0
        while not self.is_correct(answer):
            n_guesses += 1
            if n_guesses == 3:
                self.give_hint()
                n_guesses = 0
            answer = input('Try again')


class RiddleGame:

    def __init__(self):
        self.riddles = [
            Riddle(
                question='How many milk dispensers are in the canteen?',
                solution='8',
                hints=['It is above 5', 'It is less then 10'],
            ),
            Riddle(
                question='How many sugar-free chai latte options are there in the Café? (check sign in the café)',
                solution='2',
                hints=["There ain't too many"],
            ),
            Riddle(
                question='How many vending machines are there on this floor?',
                solution='2',
                hints=['Hint: not a lot'],
            ),
            Riddle(
                question='How many trees are in the atrium? (canteen area)',
                solution='4',
                hints=['It is an equal number', 'It is less than 5'],
            ),
            Riddle(
                question='How many flag poles are located in front of Nordea? (At the driveway of the main entrance)',
                solution='3',
                hints=[''],
            ),
        ]

    def start(self):
        if input(
                'Hello there.\n\n'
                'I am the Riddler and I need to be solved.\n\n'
                'Are you intrigued? (y/n)\n'
        ) == 'y':
            print_separator()
            print('In order to solve me you have to answer the following questions:\n\n{}'.format(
                '\n'.join(riddle.question for riddle in self.riddles), end='\n'))
            print_separator()
            while self.riddles:
                riddle = self.riddles.pop(0)
                riddle.start()

            print_separator()
            print('Well done. You made it!'.format(line='=' * 30))


if __name__ == '__main__':
    riddle_game = RiddleGame()
    riddle_game.start()
